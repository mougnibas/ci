```
© Copyright 2019 Yoann MOUGNIBAS

This file is part of ci.

ci is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ci is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ci. If not, see <http://www.gnu.org/licenses/>
```

# Project informations

## General

The purpose of this project is to provide a docker image (including "docker",
"maven" and "java") to be used in ci integration.

## How to use this image ?

### From gitlab-ci (gitlab.com shared runner)

In your `.gitlab-cy.yml` file :

```
image: mougnibas/gitlab-ci-docker

variables:
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2

services:
  - docker:dind
```

You're now ready to go to invoque maven and docker in your jobs script.

### From a docker enabled machine

Example from your source directory (Windows Powershell) :

```
docker run -it --name my-ci -v /var/run/docker.sock:/var/run/docker.sock -v "$(pwd):/mnt/sources" mougnibas/ci bash
cd /mnt/sources
mvn clean package
docker build .
```
