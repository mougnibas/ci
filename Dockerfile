# © Copyright 2019 Yoann MOUGNIBAS
#
# This file is part of ci.
#
# ci is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ci is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ci. If not, see <http://www.gnu.org/licenses/>

#
# Use debian 9 (stretch) as base image
#
FROM debian:9

#
# In a single layer, run :
# - install wget from apt-get
# - install OpenJDK 12, from binary tarball
# - install Maven 3.6.1, from binary tarball
# - install docker (and requirements), from docker repository
# - make some clean up
#
RUN                                                                           \
  apt-get update                                                           && \
  apt-get install -y wget                                                  && \
                                                                              \
  cd /root                                                                 && \
  wget https://download.java.net/java/GA/jdk12.0.1/69cfe15208a647278a19ef0990eea691/12/GPL/openjdk-12.0.1_linux-x64_bin.tar.gz && \
  tar -xf openjdk-12.0.1_linux-x64_bin.tar.gz                              && \
  rm openjdk-12.0.1_linux-x64_bin.tar.gz                                   && \
  ln -s /root/jdk-12.0.1/bin/java /usr/local/bin/                          && \
                                                                              \
  wget http://apache.mirrors.ovh.net/ftp.apache.org/dist/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.tar.gz && \
  tar -xf apache-maven-3.6.1-bin.tar.gz                                    && \
  rm apache-maven-3.6.1-bin.tar.gz                                         && \
  ln -s /root/apache-maven-3.6.1/bin/mvn /usr/local/bin/                   && \
                                                                              \
  apt-get install -y                                                          \
    apt-transport-https                                                       \
    ca-certificates                                                           \
    curl                                                                      \
    gnupg2                                                                    \
    software-properties-common                                             && \
  curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -  && \
  add-apt-repository                                                          \
    "deb [arch=amd64] https://download.docker.com/linux/debian                \
    $(lsb_release -cs)                                                        \
    stable"                                                                && \
  apt-get update                                                           && \
  apt-get install -y docker-ce docker-ce-cli containerd.io                 && \
                                                                              \
  apt-get remove -y wget                                                   && \
  apt-get autoremove -y                                                    && \
  apt-get purge                                                            && \
                                                                              \
  hash -r
